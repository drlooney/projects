﻿<?php include "config.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<title><?= $site_name ?></title>
	<link rel="shortcut icon" href="" /><!-- Favicon -->
	<link rel="stylesheet" href="styles.css" type="text/css" /><!-- Styl CSS -->
</head>
<body>
    <div id="header">
        <div class="subContainer">
            <div id="logo">
            <img src="images/logo.png" /><!-- /logo -->
            </div>
        </div>
    </div>
    
    <div id="navigation">
    <ul>
        <li><a href="index.php" title="Statystyki">Statystyki</a></li>
		<li><a href="<?= $forum_link ?>" title="Forum">Forum</a></li>
		<li><a href="index.php?p=2" title="Lista graczy">Lista graczy</a></li>
        <li><a href="index.php?p=3" title="Toplisty">Toplisty</a></li>
		<li><a href="index.php?p=4" title="Wyścigi">Wyścigi</a></li>
        <li><a href="index.php?p=5" title="Lista banów">Lista banów</a></li>
    </ul>
    </div>
    <div id="bgcont">
    <div id="container">            
    <div id="primaryContent">
<?php 
mysql_connect($db_host, $db_user, $db_pass);
mysql_select_db($db_name);

if(isset($_GET['p']))
{
	$page = $_GET['p'];
}else $page = 0;

if($page >= 0 && $page <= 5)
{
	$title = array("Statystyki", "Lista Graczy", "Toplisty", "Rekordy wyścigów", "Lista banów");
	echo "<h2>". $title[$page] ."</h2><p>";
	$files = array("stats.php", "players.php", "toplist.php", "races.php", "bans.php");

	include "pages/". $files[$page];

	echo "\n</p>";
}
else
{
	echo "<h2>Nieprawidłowa strona!</h2>";
}
?>

      
</div>
<br class="clear" />    
</div>
    
<br class="clear" />   
</div>
</body>
</html>
