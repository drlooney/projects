<?php
class public_panel_character_createchar2 extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Niestety, nie jesteś zalogowany dlatego dostęp do tej części forum został zablokowany.',0);
		}
		
		switch( $this->request['do'] )
		{
			case 'newchar':
		if($this->request['age']>90 || $this->request['age']<18)
		{
			$messages[] = 'Podany wiek wykracza poza dozwolony zakres.';
		}
		else
		{
			$newCharacter['age'] = intval($this->request['age']);
		}
		
		if(is_numeric($this->request['skin']))
		{
			$newCharacter['skin'] = intval($this->request['skin']);
		}
		else
		{
			$messages[] = 'Skin niepoprawny.';
		}
		
		if($this->request['gender'] != 2 && $this->request['gender'] != 1)
		{
			$messages[] = 'Nie wybrano płci postaci lub formularz został niepoprawnie pobrany.';
		}
		else
		{
			$newCharacter['sex'] = intval($this->request['gender']);
		}
		
		if(!empty($this->request['name']) && !preg_match('~[A-Z][a-z]*~',$this->request['name']) && preg_match('~[A-Z][a-z]*~',$this->request['surname']))
		{
			$messages[] = 'Podana nazwa postaci nie jest dozwolona.';
		}
		else
		{
			$newCharacter['nick'] = $this->request['name'] . '_' . $this->request['surname'];
		}
		
		$this->DB->query("SELECT * FROM fc_skins WHERE `model` = ".mysql_escape_string($newCharacter['skin'])." AND `sex` = ".mysql_escape_string($newCharacter['sex'])."");
		$this->DB->execute();
		if($this->DB->getTotalRows() <= 0)
		{
			$messages[] = 'Wybrany skin nie znajduje się w liście wyboru.';
		}
		
		$this->DB->query("SELECT name FROM fc_characters WHERE name LIKE '".$this->DB->addSlashes($newCharacter['nick'])."'");
		$this->DB->execute();
		if($this->DB->getTotalRows() > 0)
		{
			$existing = $this->DB->fetch();
			$messages[] = 'Podana nazwa postaci jest zbyt podobna do już istniejącej w świecie gry ('.$existing['name'].').';
		}
		
		$db = ipsRegistry::DB();
		$db->query('SELECT player_uid FROM fc_characters, ipb_members WHERE member_id='.$this->memberData['member_id'].' AND global_uid=member_id AND (premium<='.IPS_UNIX_TIME_NOW.' AND !(block & 1) AND hours<10) OR member_id='.$this->memberData['member_id'].' AND global_uid=member_id AND (!(block & 1) AND hours<3)');
		
		
		if($db->getTotalRows() > 0)
		{
			$this->registry->output->showError('Nie można założyć nowej postaci, możliwe powody:<br /><ul class="ipsPad_top bullets"><li><b>Któraś z Twoich postaci może mieć poniżej 10h (konta premium: 3h). <a href="index.php?app=panel">Wróc do listy postaci.</a></b></li></ul>', 0);
			return;
		}
		
		if(count($messages))
		{
			$this->registry->output->showError('Podczas rejestracji popełniono następujące błędy:<br /><br /><ul><li>'.implode('</li><li>',$messages).'</li></ul>',0);
		}
		else
		{
			$this->DB->query(sprintf(
				'INSERT INTO fc_characters (name,sex,age,last_skin,cash,global_uid,health) VALUES(\'%s\',%d,%d,%d,500,%d,100)',
				$this->DB->addSlashes($newCharacter['nick']),
				$newCharacter['sex'] - 1,
				$newCharacter['age'],
				$newCharacter['skin'],
				$this->memberData['member_id']));
			
			$this->DB->execute();
			$this->registry->output->silentRedirect(
				$this->registry->output->buildUrl('/index.php?app=panel&module=character','publicWithApp')
			);
		}
				break;
		}

		$template = $this->registry->output->getTemplate('panel')->panel_createchar($messages);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('Tworzenie postaci');
		$this->registry->output->addNavigation( 'Tworzenie postaci', 'app=panel&module=character&section=createchar' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>