<?php
class public_panel_character_list extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Niestety, nie jesteś zalogowany dlatego dostęp do tej części forum został zablokowany.',0);
		}

			$this->DB->query('SELECT * FROM `fc_characters` WHERE `global_uid` = '.$this->memberData['member_id'].'');	
			$this->DB->execute();
				
		while($row = $this->DB->fetch())
		{	
			//$row['hours'] = floor($row['hours'] / 3600);
			//$row['minutes'] = floor(($row['minutes'] - floor($row['minutes'] / 3600) * 3600) / 60);
			$row['name'] = str_replace("_", " ", $row['name']);			
			$chars[] = $row;
		}
		
		//Aplikacje na postać
		$this->DB->query('SELECT * FROM `panel_applications` WHERE `owner_app` = '.intval($this->memberData['member_id']).'');
		$this->DB->execute();
				
		while($row = $this->DB->fetch())
		{	
			switch($row['status'])
			{
				case 1: $row['status'] = '<font color="orange"><b>Oczekuj&#281; na weryfikacj&#281;</b></font>'; break;
				case 2: $row['status'] = '<font color="green"><b>Zaakceptowana</b></font>'; break;
				case 3: $row['status'] = '<font color="red"><b>Odrzucona</b></font>'; break;
				case 4: $row['status'] = '<font color="red"><b>Zablokowana</b></font>'; break;
			}
			
			$app[] = $row;
		}
		
		//Ukrywanie postaci w profilu
		if(isset($this->request['hideChar']))
		{ 
			$this->DB->query('SELECT `global_uid`, `hide`, `player_uid` FROM `fc_characters` WHERE `player_uid` = '.$this->request['uid'].'');
			$row = $this->DB->fetch();
			
			if($this->memberData['member_group_id'] != 4 || $this->memberData['member_group_id'] != 7)
			{
				if($row['global_uid'] != $this->memberData['member_id'])
				{
					$this->registry->output->showError('Wybrana postać nie jest przypisana do Twojego konta globalnego.',0);
				}
				
				if($this->memberData['premium'] <= IPS_UNIX_TIME_NOW)
				{
					$this->registry->output->redirectScreen("", "index.php?app=panel&module=character&section=list&c=nopremium");
				}
			}
			
			if($row['hide'] == 0)
			{
				$hide = 1;
			}
			else
			{
				$hide = 0;
			}
			
			$this->DB->query('UPDATE `fc_characters` SET `hide` = '.$hide.'  WHERE `player_uid` = '.$row['player_uid'].'');
			$this->registry->output->silentRedirect('index.php?app=panel&module=character');
		}

		$template = $this->registry->output->getTemplate('panel')->panel_character($chars, $charss, $app);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('Lista Twoich postaci');
		$this->registry->output->addNavigation( 'Lista Twoich postaci', 'app=panel&module=character' );
		$this->registry->getClass('output')->sendOutput();
	}
}
?>