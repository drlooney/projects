<?php
class public_panel_character_logs extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Niestety, nie jesteś zalogowany dlatego dostęp do tej części forum został zablokowany.',0);
		}
			$this->DB->query('SELECT l.*, p.* FROM fc_logs l, fc_characters p WHERE l.char_id = p.player_uid AND p.global_uid = '.$this->memberData['member_id'].' ORDER BY date DESC LIMIT 20');
			$this->DB->execute();
		
		while($row = $this->DB->fetch())
		{
			$logs[] = $row;
		}

		$template = $this->registry->output->getTemplate('panel')->panel_logs($logs);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('Logi logowań');
		$this->registry->output->addNavigation( 'Logi logowań', 'app=panel&module=character&section=logs' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>