<?php
class public_panel_character_showchar extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{	
	
	    if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Niestety, nie jesteś zalogowany dlatego dostęp do tej części forum został zablokowany.',0);
		}
		
		/* Postacie */
		$this->DB->query("SELECT * FROM `fc_characters` WHERE `player_uid` = ".$this->request['uid']." LIMIT 1");
		$row = $this->DB->fetch();	
		$row['hp'] 	= sprintf("%01.2f", $row['HP']);		

		
		if($this->memberData['member_id'] != $row['global_uid'] && $this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7 && $this->memberData['member_group_id'] != 9)
		{
			$this->registry->output->showError('Nie posiadasz uprawnień do podglądu tej postaci.',0);
		}
		
        /* Przedmioty */
		
		$this->DB->query('SELECT * FROM `fc_items` WHERE `owner` = '.$this->request['uid'].' AND `ownertype` = 1 ORDER by `uid` DESC LIMIT 20');	
		$this->DB->execute();
				
		while($item = $this->DB->fetch())
		{
			switch($item['kind'])
			{
			    case 0: $item['kind'] = 'Nieokreślony'; break;
				case 1: $item['kind'] = 'Broń'; break;
				case 2: $item['kind'] = 'Paralizator'; break;
				case 3: $item['kind'] = 'Lakier'; break;
				case 4: $item['kind'] = 'Syrena'; break;
				case 5: $item['kind'] = 'Metaamfetamina'; break;
				case 6: $item['kind'] = 'Kokaina'; break;
				case 7: $item['kind'] = 'Marichuana'; break;
				case 8: $item['kind'] = 'Podpałka'; break;
				case 9: $item['kind'] = 'Krótkofalówka'; break;
				case 10: $item['kind'] = 'Megafon'; break;
				case 11: $item['kind'] = 'Wytrych'; break;
				case 12: $item['kind'] = 'Wędka'; break;
				case 13: $item['kind'] = 'Przynęta'; break;
				case 14: $item['kind'] = 'Ryba'; break;
				case 15: $item['kind'] = 'Telefon'; break;
				case 16: $item['kind'] = 'Karta kredytowa'; break;
				case 17: $item['kind'] = 'Maska'; break;
				case 18: $item['kind'] = 'Obiekt przyczepialny'; break;
				case 19: $item['kind'] = 'Tuning'; break;
				case 20: $item['kind'] = 'Nasiona'; break;
				case 21: $item['kind'] = 'Jedzenie'; break;
				case 22: $item['kind'] = 'Alkohol'; break;
				case 23: $item['kind'] = 'Papierosy'; break;
				case 24: $item['kind'] = 'Zegarek'; break;
				case 25: $item['kind'] = 'Kość do gry'; break;
				case 26: $item['kind'] = 'Paczka pieniędzy'; break;
				case 27: $item['kind'] = 'Tablica rejestracyjna'; break;
			}	
			$items[] = $item;
		}	
		/* Pojazdy */
		
		$this->DB->query('SELECT * FROM `fc_vehicles` WHERE `owner` = '.$this->request['uid'].' AND `ownertype` = 1 ORDER by `uid` DESC LIMIT 20');	
		$this->DB->execute();
				
		while($veh = $this->DB->fetch())
		{	
			$veh['distance'] 	= sprintf("%01.2f", $veh['distance']);
			$veh['HP'] 			= sprintf("%01.2f", $veh['HP']);
			$vehs[] = $veh;
		}
		
		/* Drzwi */
		
		$this->DB->query('SELECT * FROM `fc_doors` WHERE `ownertype` = 1 AND `owner` = '.$this->request['uid'].' ORDER by `uid` DESC LIMIT 20');	
		$this->DB->execute();
				
		while($door = $this->DB->fetch())
		{	
			$doors[] = $door;
		}
		
		/* Pozostałe postacie 
		
		$this->DB->query('SELECT * FROM `1postacie` WHERE `owneruid` = '.$row['owneruid'].' AND `uid` <> '.$row['uid'].' ORDER by `lastlogin` DESC LIMIT 10');	
		$this->DB->execute();
				
		while($char = $this->DB->fetch())
		{	
			$char['nick'] 	= str_replace("_", " ", $char['nick']);	
			$chars[] = $char;
		}
	
		
		$row['nick'] = str_replace("_", " ", $row['nick']);
		if($this->memberData['member_id'] != $row['owneruid'] && $this->memberData['member_group_id'] != 4)
		{
			$this->registry->output->showError('Nie posiadasz uprawnień do podglądu tej postaci.',0);
		}
		if($row['control'] == 0)
		{
			$this->registry->output->showError('Taka postać nie istnieje.',0);
		}
		/*
		if($this->request['spawn'] != 0)
		{
			if($row['owneruid'] == $this->memberData['member_id'] || $this->memberData['member_group_id'] == 4)
			{
				$this->DB->query('UPDATE `1postacie` SET `spawnid` = '.$this->request['spawn'].', `Spawn` = 1 WHERE `uid` = '.strval($row['uid']).'');
			}
		}
		*/
		$template = $this->registry->output->getTemplate('panel')->panel_showchar($row, $logs, $vehs, $chars, $doors, $items);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('Postać '.str_replace("_", " ", $row['name']).'');
		$this->registry->output->addNavigation( 'Postać '.str_replace("_", " ", $row['name']).'', 'app=panel&modules=character&section=showchar&uid='.$this->request['uid'].'' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?> 