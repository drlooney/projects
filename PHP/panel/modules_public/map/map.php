<?php

 class public_panel_map_map extends ipsCommand
 {
	public function doExecute(ipsRegistry $registry)
	{
		if($this->request['list'])
		{
			$this->DB->query('SELECT d.uid,d.name,d.enterx,d.entery,g.kind FROM fc_doors d LEFT JOIN fc_groups g ON (d.owner=g.uid) WHERE d.ownertype=2 AND d.entervw=0');
			$this->DB->execute();

			$mapList = '{"items":[';
			while($data = $this->DB->fetch())
			{
				if($data['group_type'] != NULL || $data['group_type'] != 19 || $data['group_type'] != 20)
				{
					$data['door_x'] = floor($data['door_enterx']);
					$data['door_y'] = floor($data['door_entery']);
					$mapList .= '{"icon":"'.$data['group_type'].'","id":'.$data['uid'].',"name":"'.$data['name'].'","pos":{"x":'.$data['door_x'].',"y":'.$data['door_y'].'},"text":"Adres: SA '.$data['uid'].'"},';
				}
			}

			$mapList .= ']}';
			print $mapList;
		}
		else
		{
			$template = $this->registry->output->getTemplate('panel')->panel_map();

			$this->registry->output->addContent($template);
			$this->registry->output->setTitle('Mapa świata');
			$this->registry->output->addNavigation('Mapa świata', 'app=panel&module=map&section=map', 'false', 'game_map', 'public');
			$this->registry->output->sendOutput();
		}
	}
 }