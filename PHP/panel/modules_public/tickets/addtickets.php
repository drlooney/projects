<?php
class public_panel_tickets_addtickets extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{	
		/* Zabezpieczenia. */
		if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Nie jesteś zalogowany.',0);
		}

		/* Edytor */
	    $classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/editor/composite.php', 'classes_editor_composite' );
	    $editor = new $classToLoad();
	
	    $editor->setAllowBbcode( true );
	    $editor->setAllowSmilies( true );
	    $editor->setAllowHtml( false );
	    
	    if( method_exists( $editor, 'setForceRte' ) )
	    {
	        $editor->setForceRte( true );
	    }
	    
	    if( method_exists( $editor, 'setRteEnabled' ) )
	    {
	        $editor->setRteEnabled( true );
	    }
	    
	    if( method_exists( $editor, 'setLegacyMode' ) )
	    {
	        $editor->setLegacyMode( false );
	    }
	
	    /* Set content in editor */
	    $edytor = $editor->show( 'Post', array(), $db[ 'content' ] );
		
		/* Koniec pobierania, konfiguracja końcowa. */
		$template = $this->registry->output->getTemplate('panel')->panel_tickets_add($edytor);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('Zgłoszenia');
		$this->registry->output->addNavigation( 'Zgłoszenia', 'index.php?app=panel&module=tickets&section=tickets' );
		/* Zgłoszenia */
		$this->registry->output->addNavigation( 'Dodaj zgłoszenie', 'index.php?app=panel&module=tickets&section=addtickets' );
		/* koniec */
		$this->registry->getClass('output')->sendOutput();
	}	
}
?> 