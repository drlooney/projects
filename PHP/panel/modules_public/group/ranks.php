<?php

class public_panel_group_ranks extends ipsCommand
{
	public function doExecute( ipsRegistry $registry )
	{
		require_once( IPSLib::getAppDir( 'panel' ) . '/sources/group.php' );
		$this->registry->setClass( 'group', new group( $registry ) );
		$group = $this->registry->getClass( 'group' );
		
		$group->checkCharacterAndGroup( intval( $this->request['group'] ), intval( $this->request['character'] ), $this->memberData['member_id'] );
		$information = $group->fetchGroupInformation( intval( $this->request['group'] ) );

		$ranks = $this->fetchRanks( $this->request['group'] );
		$skins = $this->fetchSkins( $information['kind'] );

		$permissions = array(
			1 => '<span data-tooltip="Zarządzanie pracownikami"> P </span>',
			8 => '<span data-tooltip="Zarządzanie pojazdami"> V </span>',
			16 => '<span data-tooltip="Zarządzanie drzwiami"> D </span>',
			128 => '<span data-tooltip="Dostęp do pojazdów"> DV </span>',
			512 => '<span data-tooltip="Dostęp do chatu OOC"> OOC </span>',
		);

		if( isset( $this->request['updateRanks'] ) )
		{
			$this->updatePermissions();
		}

		if( isset( $this->request['add_rank'] ) )
		{
			$this->addRank( $group, $this->request['rank_name'] );
		}

		$this->registry->output->setTitle( 'Zarządzanie grupami' );
		$this->registry->output->addNavigation( ''. $information['desc'] .' (UID: '. $information['uid'] .')', 'app=rp&module=group&section=ranks&group='. $this->request['group'] .'&character='. $this->request['character'] );
		$this->registry->output->addContent( $this->registry->output->getTemplate( 'panel' )->panel_group_ranks( $ranks, $permissions, $skins ) );
		$this->registry->output->sendOutput();
	}

	protected function addRank( $group, $desc )
	{
		if( $group->getGroupRanks( $this->request['group'] ) < 10)
		{
			if( $desc )
			{
				$this->DB->query('INSERT INTO `fc_ranks` VALUES (NULL, ' . $this->request['group'] . ', "' . $desc . '", 0, 0, 0)');
			
				$this->registry->output->redirectScreen( 'Ranga (' . $desc . ') została pomyślnie dodana.', $this->settings['base_url'] . 'app=panel&module=group&section=ranks&group=' . $this->request['group'] . '&character=' . $this->request['character'] . '' );
			}
			else
			{
				$this->registry->output->showError('Nazwa rangi nie została pomyślnie wprowadzona.',0);
			}
		}
		else
		{
			$this->registry->output->showError('W grupie znajduje się zbyt dużo rang, maksymalna ilość to <strong>10</strong>.', 0);
		}
	}

	protected function fetchRanks( $group )
	{
		$int = 1;

		$this->DB->query('SELECT * FROM `fc_ranks` WHERE `group_uid` = ' . $group . '');
		
		while( $row = $this->DB->fetch() )
		{
			$row['num'] = $int;
			$int ++;

			$ranks[] = $row;
		}
		
		return $ranks;
	}

	protected function fetchSkins( $kind )
	{
		$this->DB->query('SELECT * FROM `fc_group_skins` WHERE `group_type` = ' . $kind . '');
		
		while( $row = $this->DB->fetch() )
		{
			$skins[] = $row;
		}
		
		return $skins;
	}

	public function updatePermissions()
	{
		foreach($this->request['perm'] as $member => $locglob)
		{
			if(is_array($locglob['global']))
			{
				$updateMember[$member] = $this->packMemberPermissions( $locglob['global'] );
			}
		}
					
		foreach($updateMember as $rank_id => $perm)
		{		
			$this->DB->query('UPDATE `fc_ranks` SET `permissions` = ' . intval( $perm ) . ' WHERE `uid` = ' . intval( $rank_id ) . ' ');
		}

		foreach( $this->request['rank'] as $id => $value)
		{
			$this->DB->query('UPDATE `fc_ranks` SET `skin` = ' . intval( $value['skin'] ) . ', `payout` = ' . intval( $value['payout'] ) . ', name = "' . $this->DB->addSlashes( $value['name'] ) . '" WHERE `uid` = ' . $id . '');
		}

		$this->registry->output->redirectScreen( 'Ustawienia zostały zapisane.', $this->settings['base_url'] . 'app=panel&module=group&section=ranks&group=' . $this->request['group'] . '&character=' . $this->request['character'] . '' );
	}

	public function packMemberPermissions( $newGlobalPermissionSet )
	{	
		$return = array();
		
		if( count( $newGlobalPermissionSet ) )
		{
			foreach($newGlobalPermissionSet as $key => $val)
			{
				if($val)
				{
					$return |= intval( $key );
				}
				else $return = 0;
			}
		}
		
		return $return;
	}
}