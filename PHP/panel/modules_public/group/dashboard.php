<?php

class public_panel_group_dashboard extends ipsCommand
{
	public function doExecute( ipsRegistry $registry )
	{
		require_once( IPSLib::getAppDir( 'panel' ) . '/sources/group.php' );
		$this->registry->setClass( 'group', new group( $registry ) );
		$group = $this->registry->getClass( 'group' );
		
		$group->checkCharacterAndGroup( intval( $this->request['group'] ), intval( $this->request['character'] ), $this->memberData['member_id'] );
		$information = $group->fetchGroupInformation( intval( $this->request['group'] ) );

		$vehicles = $this->fetchVehicles( $group, $this->request['group'] );

		$this->registry->output->setTitle( 'Zarządzanie grupami' );
		$this->registry->output->addNavigation( $information['desc'] .' (UID: '. $information['uid'] .')', 'app=panel&module=group&section=dashboard&group='. $this->request['group'] .'&character='. $this->request['character'] );
		$this->registry->output->addContent( $this->registry->output->getTemplate( 'panel' )->panel_group_dashboard( $information, $vehicles ) );
		$this->registry->output->sendOutput();
	}

	public function fetchVehicles( $group, $id )
	{
		$this->DB->query('SELECT * FROM `fc_vehicles` WHERE `ownertype` =  2 AND `owner` = ' . $id . '');
		
		while( $row = $this->DB->fetch() )
		{
			//$row['model'] = $group->GetVehicleName( $row['model'] );
			$vehicles[] = $row;
		}

		return $vehicles;
	}
}