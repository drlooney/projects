<?php

class public_panel_group_members extends ipsCommand
{
	public function doExecute( ipsRegistry $registry )
	{
		require_once( IPSLib::getAppDir( 'panel' ) . '/sources/group.php' );
		$this->registry->setClass( 'group', new group( $registry ) );
		$group = $this->registry->getClass( 'group' );
		
		$group->checkCharacterAndGroup( intval( $this->request['group'] ), intval( $this->request['character'] ), $this->memberData['member_id'] );
		$information = $group->fetchGroupInformation( intval( $this->request['group'] ) );

		$members = $this->fetchMembers( $this->request['group'] );
		$subgroups = $this->fetchSubGroups( $information['uid'] );
		$ranks = $this->fetchRanks( $information['uid'] );

		if(isset( $this->request['updateMembers'] ))
		{	
			$this->UpdateMembers( $group );
		}

		if(isset( $this->request['add'] ))
		{
			$this->addMember( $this->request['char_name'], $this->request['group'] );
		}

		$this->registry->output->setTitle( 'Zarządzanie pracownikami' );
		$this->registry->output->addNavigation( ''. $information['desc'] .' (UID: '. $information['uid'] .')', 'app=panel&module=group&section=members&group='. $this->request['group'] .'&character='. $this->request['character'] );
		$this->registry->output->addContent( $this->registry->output->getTemplate( 'panel' )->panel_group_members( $information, $members, $subgroups, $ranks ) );
		$this->registry->output->sendOutput();
	}

	protected function addMember( $character, $group )
	{
		$this->DB->query('SELECT `player_uid`, `logged` FROM `fc_characters` WHERE `name` = "' . mysql_escape_string( $character ) . '"');
		$char_id = $this->DB->fetch();
		
		if( $char_id )
		{
			$this->DB->query('SELECT * FROM `fc_member_groups` WHERE `player_uid` = ' . strval( $char_id['player_uid'] ) . '');
		
			if( $this->DB->getTotalRows() <= 3 || ! $char_id['logged'] )
			{
				$this->DB->query('SELECT * FROM `fc_member_groups` WHERE `player_uid` = ' . strval( $char_id['player_uid'] ) . ' AND `group_id` = ' . $group . '');
				
				if( ! $this->DB->getTotalRows() )
				{
					$this->DB->query('INSERT INTO `fc_member_groups` (`group_id`, `player_uid`) VALUES (' . $group . ', ' . $char_id['player_uid'] . ')');
					
					$this->registry->output->redirectScreen( 'Nowy pracownik został dodany.', $this->settings['base_url'] . 'app=rp&module=group&section=members&group=' . $this->request['group'] . '&character=' . $this->request['character'] . '' );
				}
				else $this->registry->output->showError('Wybrany pracownik należy już do tej grupy.',0);
			}
			else $this->registry->output->showError('Wybrany pracownik posiada maksymalną ilość grup lub znajduje się aktualnie w grze.',0);
		}
		else $this->registry->output->showError('Wybrany pracownik nie został odnaleziony w bazie danych.',0);
	}

	protected function UpdateMembers( $group )
	{
		$group->checkCharacterAndGroup( intval( $this->request['group'] ), intval( $this->request['character'] ), $this->memberData['member_id'] );

		foreach($this->request['member'] as $member=>$newData)
		{
			$this->DB->query('UPDATE `fc_member_groups` SET `subgroup` = ' . intval($newData['subgroup']) . ', `rank_uid` = "' . intval($newData['rank_uid']) . '" WHERE `player_uid` = ' . intval($member) . ' AND `group_id` = ' . $this->request['group'] . '');
		}

		$this->registry->output->redirectScreen( 'Ustawienia zostały zapisane.', $this->settings['base_url'] . 'app=panel&module=group&section=members&group=' . $this->request['group'] . '&character=' . $this->request['character'] . '' );
	}

	protected function fetchMembers( $group )
	{
		$this->DB->query('SELECT g.member_id, g.members_seo_name, g.members_display_name, g.member_group_id, c.global_uid, c.name, m.* FROM fc_characters c, fc_member_groups m, ipb_members g WHERE c.global_uid = g.member_id AND c.player_uid = m.player_uid AND m.group_id = ' . $group . '');
		
		while( $row = $this->DB->fetch() )
		{
			$row['name'] = str_replace("_", " ", $row['name']);

			$row['group_hours'] = floor(($row['time'] / 60) / 60);;
			$row['group_minutes'] = intval($row['time'] % 60);

			$members[] = $row;
		}
		
		return $members;
	}

	protected function fetchSubGroups( $group )
	{
		$this->DB->query('SELECT * FROM `fc_subgroups` WHERE `group_id` = ' . $group . '');
		
		while( $row = $this->DB->fetch() )
		{
			$subgroups[] = $row;
		}
		
		return $subgroups;
	}

	protected function fetchRanks( $group )
	{
		$this->DB->query('SELECT * FROM `fc_ranks` WHERE `group_uid` = ' . $group . '');
		
		while( $row = $this->DB->fetch() )
		{
			$ranks[] = $row;
		}
		
		return $ranks;
	}
}