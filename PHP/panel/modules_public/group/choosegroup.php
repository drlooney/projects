<?php
class public_panel_group_choosegroup extends ipsCommand
{	
	public function doExecute( ipsRegistry $registry ) 
	{
		if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Nie jesteś zalogowany, zrób to aby otrzymać dostęp do tej części forum.',0);
		}

			$this->DB->query('SELECT `global_uid` FROM `fc_characters` WHERE `player_uid` = '.strval($this->request['character']).'');
			$this->DB->execute();
			$char = $this->DB->fetch();
			
		if($char['global_uid'] != $this->memberData['member_id'])
		{
			$this->registry->output->showError('Nie jesteś zalogowany, zrób to aby otrzymać dostęp do tej części forum.',0);
		}
			
			$this->DB->query('SELECT * FROM `fc_member_groups` WHERE `player_uid` = '.strval($this->request['character']).'');
			$this->DB->execute();
			
			while($row = $this->DB->fetch())
			{	
				$groups[] = $row;
			}
		
		if(isset($this->request['choose_group']))
		{
			if(!$this->request['choice'])
			{
				$this->registry->output->showError('Forumlarz został źle pobrany, pobierz go ponownie.',0);
			}
			else
			{
				if(is_numeric($this->request['choice']))
				{
					$this->registry->output->silentRedirect( $this->registry->output->buildUrl('app=panel&module=group&section=dashboard&group='.$this->request['choice'].'&character='.$this->request['character'].'') );
				}
				else
				{
					$this->registry->output->showError('Forumlarz został źle pobrany, pobierz go ponownie.',0);
				}
			}
		}
		
		$template = $this->registry->output->getTemplate('panel')->panel_group_choose($groups);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('Wybór grupy');
		$this->registry->output->addNavigation( 'Wybór grupy', 'app=panel&module=group' );
		$this->registry->getClass('output')->sendOutput();
	}

	
}
?>