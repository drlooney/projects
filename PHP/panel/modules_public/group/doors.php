<?php

class public_panel_group_doors extends ipsCommand
{
	public function doExecute( ipsRegistry $registry )
	{
		require_once( IPSLib::getAppDir( 'panel' ) . '/sources/group.php' );
		$this->registry->setClass( 'group', new group( $registry ) );
		$group = $this->registry->getClass( 'group' );
		
		$group->checkCharacterAndGroup( intval( $this->request['group'] ), intval( $this->request['character'] ), $this->memberData['member_id'] );
		$information = $group->fetchGroupInformation( intval( $this->request['group'] ) );

		$doors = $this->fetchDoors( $information['uid'] );

		$this->registry->output->setTitle( 'Zarządzanie drzwiami' );
		$this->registry->output->addNavigation( ''. $information['desc'] .' (UID: '. $information['uid'] .')', 'app=rp&module=group&section=doors&group='. $this->request['group'] .'&character='. $this->request['character'] );
		$this->registry->output->addContent( $this->registry->output->getTemplate( 'panel' )->panel_group_doors( $doors ) );
		$this->registry->output->sendOutput();
	}

	protected function fetchDoors( $group )
	{
		$this->DB->query('SELECT * FROM `fc_doors` WHERE `ownertype` = 2 AND `owner` = ' . $group . '');
		
		while( $row = $this->DB->fetch() )
		{
			if( $row['block'] )
			{
				$row['_status'] = "<font color='darkred'>Zablokowane</font>";
			}
			else
			{
				if( $row['lock'] )
				{
					$row['_status'] = "<font color='navy'>Zamknięte</font>";
				}
				else
				{
					$row['_status'] = "<font color='green'>Otwarte</font>";
				}
			}

			$doors[] = $row;
		}
		
		return $doors;
	}
}