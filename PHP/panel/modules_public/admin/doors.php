<?php
class public_panel_admin_doors extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7 && $this->memberData['member_group_id'] != 9)
		{
			$this->registry->output->silentRedirect('index.php');
		}
		
		$this->DB->query(sprintf('SELECT * FROM `fc_doors` LIMIT %d,10',$this->request['st']));	
		$this->DB->execute();	

		while($row = $this->DB->fetch())
		{		
			switch($row['ownertype'])
			{
				case 0: $row['ownertype'] = 'Nieznany'; break;
				case 1:	$row['ownertype'] = 'Gracz'; break;
				case 2:	$row['ownertype'] = 'Grupa'; break;
				case 3:	$row['ownertype'] = 'Dom'; break;
				case 4: $row['ownertype'] = 'Sklep'; break;
				case 5: $row['ownertype'] = 'Areszt'; break;
				case 6: $row['ownertype'] = 'Magazyn broni'; break;
				case 7: $row['ownertype'] = 'Hotel'; break;
				case 8: $row['ownertype'] = 'Bank'; break;
				case 10: $row['ownertype'] = 'Praca'; break;
				case 11: $row['ownertype'] = 'Bankomat'; break;
				case 12: $row['ownertype'] = 'Przejście w domu'; break;
				case 13: $row['ownertype'] = 'Magazyn gangu'; break;
				case 14: $row['ownertype'] = 'Paczki'; break;
				case 15: $row['ownertype'] = 'Magazyn narkotyków'; break;
				default: $row['ownertype'] = 'Nieznany'; break;
			}
			
			$doors[] = $row;
		}
		
			$count = $this->DB->query('SELECT COUNT(*) as max FROM fc_doors ORDER BY uid ASC');
			$count = $this->DB->fetch($count);
			
			$pagination = $this->registry->getClass('output')->generatePagination( array( 
																		'totalItems'		=> $count['max'],
																		'itemsPerPage'		=> 10,
																		'baseUrl'			=> "app=panel&module=admin&section=doors",
																		)
																);
		
		$template = $this->registry->output->getTemplate('panel')->panel_admin_doors($doors, $pagination);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('ACP');
		$this->registry->output->addNavigation( 'ACP', 'app=panel&module=admin&section=doors' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>