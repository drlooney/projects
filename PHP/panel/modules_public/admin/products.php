<?php
class public_panel_admin_products extends ipsCommand
{
	public function doExecute( ipsRegistry $registry )
	{	
			if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 7)
			{
				$this->registry->output->silentRedirect('index.php');
			}
			
			echo '<h3 class="maintitle">Lista produktów grupy</h3>';
			
			echo '<table class="ipb_table">
					<tbody>
						<tr class="header">
							<th scope="col" class="short">Nazwa produktu</th>
							<th scope="col" class="short">Ilość</th>
							<th scope="col" class="short">Cena hurtowa</th>
							<th scope="col" class="short">Cena sprzedaży</th>
							<th scope="col" class="short">Prowizja dla pracownika</th>
						</tr>';
			$licz = $this->DB->query('SELECT * FROM `fc_orders_products` WHERE `ownertype` = 2 AND `owner` = '.$this->request['uid'].'');
			
			if($this->DB->getTotalRows( $licz ))
			{
				$this->DB->query('SELECT * FROM `fc_orders_products` WHERE `ownertype` = 2 AND `owner` = '.$this->request['uid'].'');
				while($row = $this->DB->fetch())
				{	
					echo '<tr>
							<th class="short"> '.$row['name'].'</th>
							<th class="short">'.$row['value'].'</th>
							<th class="short"><font color="green"><b>$</b></font> '.$row['price'].'</th>
							<th class="short"><font color="green"><b>$</b></font> '.$row['sell'].'</th>
							<th class="short"><font color="green"><b>$</b></font> '.$row['assistant'].'</th>
						  </tr>';
				}
			}
			else
			{
				echo '<tr colspan="4"><td>Nie znaleziono żadnych produktów w magazynie.</td></tr>';
			}
			
			echo '	</tbody>
				  </table>';
			
	}
}
?>