<?php
class public_panel_admin_editchar extends ipsCommand
{
	public function doExecute( ipsRegistry $registry )
	{	
			if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 7)
			{
				$this->registry->output->silentRedirect('index.php');
			}

			$this->DB->query('SELECT * FROM `fc_characters` WHERE `player_uid` = '.$this->request['uid'].'');
			$row = $this->DB->fetch();
			
			//BUTTON
			if(!empty($_POST['updateChar']))
			{
				$this->DB->query('INSERT INTO `panel_admin_log` (`owner`, `log`, `date`) VALUES ('.$this->memberData['member_id'].', "Edytował postać '.$row['name'].' pomyślnie.", "'.IPS_UNIX_TIME_NOW.'")');
				$this->DB->execute();
			
				$this->DB->query('UPDATE `fc_characters` SET `name` = "'.$_POST['nick'].'", `age` = '.$_POST['age'].', `sex` = '.$_POST['sex'].', `cash` = "'.$_POST['cash'].'", `skin` = '.$_POST['skin'].', `health` = '.$_POST['health'].' WHERE `player_uid` = '.$this->request['uid'].'');
				$this->registry->output->silentRedirect('index.php?&app=panel&module=admin&section=chars');
			}
			
if($row['sex'] == 0)
{
	$sex2 = 'selected="selected"';
}
else
{
	$sex1 = 'selected="selected"';
}
			
			echo '<h3 class="maintitle">Edycja postaci '.$row['name'].'</h3>
<form method="post" action="index.php?app=panel&module=admin&section=editchar&uid='.$this->request['uid'].'">
			<fieldset class="ipsSettings_section">
					<div>
						<ul>
							
								<li class="custom">
	<label for="field_1" class="ipsSettings_fieldtitle">Imię Nazwisko</label>
	<input type="text" size="20" class="input_text" name="nick" value="'.$row['name'].'">
		
	
</li>

								<li class="custom">
	<label for="field_1" class="ipsSettings_fieldtitle">Zdrowie</label>
	<input type="text" size="20" class="input_text" name="health" value="'.$row['health'].'">
		
	
</li>
							

								<li class="custom">
	<label for="field_2" class="ipsSettings_fieldtitle">Stan konta</label>
	<input type="text" size="5" class="input_text" name="cash" value="'.$row['cash'].'">
		
	
</li>
 							    <li class="custom">
	<label for="field_3" class="ipsSettings_fieldtitle">Wiek</label>
	<input type="text" size="5" class="input_text" name="age" value="'.$row['age'].'">
		
	
</li>
								<li class="custom">
	<label for="field_3" class="ipsSettings_fieldtitle">Płeć</label>
	<select class="input_select" name="sex">
		<option value="0" '.$sex1.'>Mężczyzna</option>
		<option value="1" '.$sex2.'>Kobieta</option>
	</select>
		
	
</li>
								<li class="custom">
	<label for="field_3" class="ipsSettings_fieldtitle">Ubranie (skin)</label>
	<input type="text" size="5" class="input_text" name="skin" value="'.$row['skin'].'">
		
	
</li>					
	
						</ul>
					</div>
				</fieldset>
	<fieldset class="submit">
		<input type="submit" class="input_submit" name="updateChar" value="Zapisz">
	</fieldset>
</form>';
			
	}
}
?>