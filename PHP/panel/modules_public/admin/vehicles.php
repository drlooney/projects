<?php
class public_panel_admin_vehicles extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->silentRedirect('index.php');
		}
		
		$count = $this->DB->query('SELECT COUNT(*) as max FROM `fc_vehicles`');
		$count = $this->DB->fetch($count);
		
		/* Parsowanie paginacji */
		$pagination = $this->registry->getClass('output')->generatePagination( array( 
																		'totalItems'		=> $count['max'],
																		'itemsPerPage'		=> 10,
																		'baseUrl'			=> "app=panel&module=admin&section=vehicles",
																		)
																);
		
		$this->DB->query(sprintf('SELECT * FROM `fc_vehicles` ORDER by `uid` ASC LIMIT %d,10',$this->request['st']));	
		$this->DB->execute();	

		while($row = $this->DB->fetch())
		{                    
			$vehicles[] = $row;
		}
		
		$template = $this->registry->output->getTemplate('panel')->panel_admin_vehicles($pagination, $vehicles);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('ACP');
		$this->registry->output->addNavigation( 'ACP', 'app=panel&module=admin&section=vehicles' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>