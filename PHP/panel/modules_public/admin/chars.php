<?php
class public_panel_admin_chars extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->silentRedirect('index.php');
		}
		
		$count = $this->DB->query('SELECT COUNT(*) as max FROM `fc_characters`');
		$count = $this->DB->fetch($count);
		
		/* Parsowanie paginacji */
		$pagination = $this->registry->getClass('output')->generatePagination( array( 
																		'totalItems'		=> $count['max'],
																		'itemsPerPage'		=> 10,
																		'baseUrl'			=> "app=panel&module=admin&section=chars",
																		)
																);
		
		$this->DB->query(sprintf('SELECT * FROM `fc_characters` ORDER by `player_uid` ASC LIMIT %d,10',$this->request['st']));	
		$this->DB->execute();	

		while($row = $this->DB->fetch())
		{                    
			$row['name'] = str_replace("_", " ", $row['name']);
			$row['hours'] = floor($row['online'] / 3600);
			$row['minutes'] = floor(($row['online'] - floor($row['online'] / 3600) * 3600) / 60);
			$chars[] = $row;
		}
		
		$template = $this->registry->output->getTemplate('panel')->panel_admin_chars($pagination, $chars);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('ACP');
		$this->registry->output->addNavigation( 'ACP', 'app=panel&module=admin&section=chars' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>