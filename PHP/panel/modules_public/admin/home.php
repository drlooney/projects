<?php
class public_panel_admin_home extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->silentRedirect('index.php');
		}

			$this->DB->query('SELECT * FROM `panel_panel_log` ORDER by `uid` DESC LIMIT 10');	
			$this->DB->execute();	

		while($row = $this->DB->fetch())
		{                    
			$log[] = $row;
		}
		
			$this->DB->query('SELECT * FROM `panel_admin_log` ORDER by `uid` DESC LIMIT 10');	
			$this->DB->execute();	

		while($row = $this->DB->fetch())
		{                    
			$logs[] = $row;
		}
		
		$template = $this->registry->output->getTemplate('panel')->panel_admin_home($log, $logs);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('ACP');
		$this->registry->output->addNavigation( 'ACP', 'app=panel&module=admin&section=home' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>