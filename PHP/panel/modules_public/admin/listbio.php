<?php
class public_panel_admin_listbio extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->silentRedirect('index.php');
		}
		
		$count = $this->DB->query('SELECT COUNT(*) as max FROM `panel_applications`');
		$count = $this->DB->fetch($count);
		
		/* Parsowanie paginacji */
		$pagination = $this->registry->getClass('output')->generatePagination( array( 
																		'totalItems'		=> $count['max'],
																		'itemsPerPage'		=> 20,
																		'baseUrl'			=> "app=panel&module=admin&section=listbio",
																		)
																);
		
		$this->DB->query(sprintf('SELECT * FROM `panel_applications` ORDER by `a_uid` ASC LIMIT %d,20',$this->request['st']));	
		$this->DB->execute();	

		while($row = $this->DB->fetch())
		{     
            switch($row['status'])
			{
                case 0: $row['status'] = 'Brak'; break;
    			case 1: $row['status'] = 'Oczekuje'; break;
				case 2: $row['status'] = '<div style="color:green;">Zaakceptowana</div>'; break;
				case 3: $row['status'] = '<div style="color:red;">Odrzucona</div>'; break;
				case 4: $row['status'] = '<div style="color:red;">Zablokowana</div>'; break;
				default: $row['status'] = 'Błąd systemowy'; break;
			}
            		
			$applogs[] = $row;
	
		}
		
		
		
		$template = $this->registry->output->getTemplate('panel')->panel_admin_listbio($pagination, $applogs, $row);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('ACP');
		$this->registry->output->addNavigation( 'ACP', 'app=panel&module=admin&section=listbio' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>