<?php
class public_panel_admin_objects extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->silentRedirect('index.php');
		}
		
		$count = $this->DB->query('SELECT COUNT(*) as max FROM `fc_objects`');
		$count = $this->DB->fetch($count);
		
		/* Parsowanie paginacji */
		$pagination = $this->registry->getClass('output')->generatePagination( array( 
																		'totalItems'		=> $count['max'],
																		'itemsPerPage'		=> 5,
																		'baseUrl'			=> "app=panel&module=admin&section=objects",
																		)
																);
		
		$this->DB->query(sprintf('SELECT * FROM `fc_objects` ORDER by `uid` ASC LIMIT %d,5',$this->request['st']));	
		$this->DB->execute();	

		while($row = $this->DB->fetch())
		{		
			$maps[] = $row;
		}
		
		$template = $this->registry->output->getTemplate('panel')->panel_admin_objects($maps, $pagination);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('ACP');
		$this->registry->output->addNavigation( 'ACP', 'app=panel&module=admin&section=objects' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>