<?php
class public_panel_admin_global extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7 && $this->memberData['member_group_id'] != 9)
		{
			$this->registry->output->silentRedirect('index.php');
		}
		
		$count = $this->DB->query('SELECT COUNT(*) as max FROM `ipb_members`');
		$count = $this->DB->fetch($count);
		
		/* Parsowanie paginacji */
		$pagination = $this->registry->getClass('output')->generatePagination( array( 
																		'totalItems'		=> $count['max'],
																		'itemsPerPage'		=> 10,
																		'baseUrl'			=> "app=panel&module=admin&section=global",
																		)
																);
		
		$this->DB->query(sprintf('SELECT * FROM `ipb_members` ORDER by `member_id` ASC LIMIT %d,10',$this->request['st']));	
		$this->DB->execute();	

		while($row = $this->DB->fetch())
		{                    
			$members[] = $row;
		}
		
		$template = $this->registry->output->getTemplate('panel')->panel_admin_global($members, $pagination);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('ACP');
		$this->registry->output->addNavigation( 'ACP', 'app=panel&module=admin&section=global' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>