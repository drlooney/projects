<?php
class public_panel_admin_groups extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->silentRedirect('index.php');
		}
		
		$count = $this->DB->query('SELECT COUNT(*) as max FROM `fc_groups`');
		$count = $this->DB->fetch($count);
		
		/* Parsowanie paginacji */
		$pagination = $this->registry->getClass('output')->generatePagination( array( 
																		'totalItems'		=> $count['max'],
																		'itemsPerPage'		=> 10,
																		'baseUrl'			=> "app=panel&module=admin&section=groups",
																		)
																);
		
		$this->DB->query(sprintf('SELECT * FROM `fc_groups` ORDER by `uid` ASC LIMIT %d,10',$this->request['st']));	
		$this->DB->execute();	

		while($row = $this->DB->fetch())
		{
			switch($row['kind'])
			{
                case 0: $row['perm'] = 'Nieokreślona'; break;
    			case 1: $row['perm'] = 'Police Department'; break;
				case 2: $row['perm'] = 'Federal Bureau of Investigation'; break;
				case 3: $row['perm'] = 'Secret Service'; break;
				case 4: $row['perm'] = 'Government'; break;
				case 5: $row['perm'] = 'Medical Center'; break;
				case 6: $row['perm'] = 'Fire Department'; break;
				case 7: $row['perm'] = 'San News'; break;
				case 8: $row['perm'] = 'Border Patrol'; break;
				case 9: $row['perm'] = 'Mafia'; break;
				case 10: $row['perm'] = 'Gang uliczny'; break;
				case 11: $row['perm'] = 'Ściganci'; break;
				case 12: $row['perm'] = 'Syndykat'; break;
				case 13: $row['perm'] = 'Skep 24/7'; break;
				case 14: $row['perm'] = 'Warsztat'; break;
				case 15: $row['perm'] = 'Restauracja'; break;
				case 16: $row['perm'] = 'Salon samochodowy'; break;
				case 17: $row['perm'] = 'Ochrona'; break;
				case 18: $row['perm'] = 'Bank'; break;
				case 19: $row['perm'] = 'Firma taksówkarska'; break;
				default: $row['perm'] = 'Błąd systemowy'; break;
			}		
			$groups[] = $row;
		}
		
		$template = $this->registry->output->getTemplate('panel')->panel_admin_groups($groups, $pagination);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('ACP');
		$this->registry->output->addNavigation( 'ACP', 'app=panel&module=admin&section=groups' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>