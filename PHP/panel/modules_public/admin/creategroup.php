<?php
class public_panel_admin_creategroup extends ipsCommand
{
	public function doExecute( ipsRegistry $registry )
	{	
			if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 7)
			{
				$this->registry->output->silentRedirect('index.php');
			}
			
			//BUTTON
			if(!empty($_POST['createGroup']))
			{
				$this->DB->query('INSERT INTO `isantos_groups` (`desc`, `cash`, `tag`, `permission`) VALUES ("'.$this->request['desc'].'", '.$this->request['cash'].', "'.$this->request['tag'].'", '.$this->request['perm'].')');
				$this->DB->execute();
				
				$id = $this->DB->getInsertId();
				
				$this->DB->query('INSERT INTO `isantos_admin_log` (`owner`, `log`, `date`) VALUES ('.$this->memberData['member_id'].', "Stworzył nową grupę '.$this->request['desc'].' ('.$id.').", "'.IPS_UNIX_TIME_NOW.'")');
				$this->DB->execute();
			
				$this->DB->query('INSERT INTO `isantos_members_group` (`what`, `whatid`, `cash_char`, `gperm`, `player_uid`, `rank`, `name_rank`, `skin_char`) VALUES (1, '.$id.', 0, 3967, '.$this->request['player_uid'].', 255, "Lider", 0);');
				$this->DB->execute();
			
				$this->registry->output->silentRedirect('index.php?&app=panel&module=admin&section=groups');
			}
			
			echo '<h3 class="maintitle">Edycja postaci '.$row['nick'].'</h3>
<form method="post" action="index.php?app=panel&module=admin&section=creategroup">
			<fieldset class="ipsSettings_section">
					<div>
						<ul>
							
								<li class="custom">
	<label for="field_1" class="ipsSettings_fieldtitle">Nazwa grupy</label>
	<input type="text" size="20" class="input_text" name="desc" />
		
	
</li>

								<li class="custom">
	<label for="field_1" class="ipsSettings_fieldtitle">Stan konta</label>
	<input type="text" size="20" class="input_text" name="cash" />
		
	
</li>
							

								<li class="custom">
	<label for="field_2" class="ipsSettings_fieldtitle">Tag grupy</label>
	<input type="text" size="5" class="input_text" name="tag" />
		
	
</li>
								<li class="custom">
	<label class="ipsSettings_fieldtitle">Uprawnienia</label>
	<select class="input_select" name="perm">
		<option value="0">Nieokreślona</option>
		<option value="1">Police Department</option>
		<option value="2">Federal Bureau of Investigation</option>
		<option value="3">Secret Service</option>
		<option value="4">Government</option>
		<option value="5">Medical Center</option>
		<option value="6">Fire Department</option>
		<option value="7">San News</option>
		<option value="8">Border Patrol</option>
		<option value="9">Mafia</option>
		<option value="10">Gang uliczny</option>
		<option value="11">Ściganci</option>
		<option value="12">Syndykat</option>
		<option value="13">Skep 24/7</option>
		<option value="14">Warsztat</option>
		<option value="15">Restauracja</option>
		<option value="16">Salon samochodowy</option>
		<option value="17">Ochrona</option>
		<option value="18">Bank</option>
		<option value="19">Firma taksówkarska</option>	
	</select>
		
	
</li>
								<li class="custom">
	<label for="field_3" class="ipsSettings_fieldtitle">UID postaci lidera</label>
	<input type="text" size="5" class="input_text" name="player_uid" />
		
	
</li>					
	
						</ul>
					</div>
				</fieldset>
	<fieldset class="submit">
		<input type="submit" class="input_submit" name="createGroup" value="Dodaj grupę">
	</fieldset>
</form>';
			
	}
}
?>