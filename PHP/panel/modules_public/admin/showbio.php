<?php
class public_panel_admin_showbio extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{	
	
	    if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Niestety, nie jesteś zalogowany dlatego dostęp do tej części forum został zablokowany.',0);
		}
		
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->showError('Nie posiadasz uprawnień do podglądu tej postaci.',0);
		}
		
		/* Postacie */
		$this->DB->query("SELECT * FROM `panel_applications` WHERE `a_uid` = ".$this->request['uid']." LIMIT 1");
		$row = $this->DB->fetch();			
        {
		  switch($row['status'])
			{
                case 0: $row['status'] = 'Brak'; break;
    			case 1: $row['status'] = 'Oczekuje'; break;
				case 2: $row['status'] = '<font color="green">Zaakceptowana</font>'; break;
				case 3: $row['status'] = '<font color="red">Odrzucona</font>'; break;
				case 4: $row['status'] = '<font color="red">Zablokowana</font>'; break;
				default: $row['status'] = 'Błąd systemowy'; break;
			}
		}

		$template = $this->registry->output->getTemplate('panel')->panel_admin_showbio($row, $logs);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('Postać '.str_replace("_", " ", $row['uid']).'');
		$this->registry->output->addNavigation( 'Postać '.str_replace("_", " ", $row['uid']).'', 'app=panel&modules=admin&section=showbio&uid='.$this->request['uid'].'' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?> 