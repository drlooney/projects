<?php
class public_panel_admin_tickets extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{	
		/* Zabezpieczenia. */
		if($this->memberData['member_id'] != $this->request['owner'])
		{
			if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7 && $this->memberData['member_group_id'] != 9)
			{
				$this->registry->output->showError('Nie posiadasz wystarczających uprawnień do podglądu zgłoszeń.',0);
			}
		}
		/* Pobieranie danych dot. podglądanego ticketa. */
		$this->DB->query("SELECT * FROM `ipb_tickets` WHERE `t_uid` = ".$this->request['uid']." LIMIT 1");	
		$row = $this->DB->fetch();
		
		switch($row['t_type'])
		{
			case 0: $row['t_type'] = 'Niesprecyzowany'; break;
			case 1: $row['t_type'] = 'Dotyczy pojazdu'; break;
			case 2: $row['t_type'] = 'Dotyczy drzwi'; break;
			case 3: $row['t_type'] = 'Dotyczy grupy'; break;
			case 4: $row['t_type'] = 'Dotyczy pod-grupy'; break;
			case 5: $row['t_type'] = 'Dotyczy moderatora'; break;
			case 6: $row['t_type'] = 'Dotyczy administratora'; break;
			case 7: $row['t_type'] = 'Dotyczy działu / tematu'; break;
			case 8: $row['t_type'] = 'Dotyczy paneli gracza'; break;
			case 9: $row['t_type'] = 'Dotyczy błędu na forum'; break;
			case 10: $row['t_type'] = 'Dotyczy błędu w grze'; break;
		}
		switch($row['status'])
		{
			case 0: $row['status'] = 'Nowy'; break;
			case 1: $row['status'] = 'Przyjęty'; break;
			case 2: $row['status'] = 'Konwersacja'; break;
			case 3: $row['status'] = 'Realizacja'; break;
			case 4: $row['status'] = 'Rozwiązany'; break;
			case 5: $row['status'] = 'Zakończony'; break;
		}
		
		if($row['control'] == 0)
		{
			$this->registry->output->showError('Zgłoszenie o podanym identyfikatorze najprawdopodobniej nie istnieje.',0);
		}
		$this->DB->query('SELECT * FROM `ipb_tickets_post` WHERE `ticket_uid` = '.$row['t_uid'].' ORDER by `date` ASC');	
		$this->DB->execute();
		while($post = $this->DB->fetch())
		{	
			$row['last_reply'] = $post['date'];
			$row['reply'] ++;
			$posts[] = $post;
		}
		/* Edytor */
	    $classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/editor/composite.php', 'classes_editor_composite' );
	    $editor = new $classToLoad();
	
	    $editor->setAllowBbcode( true );
	    $editor->setAllowSmilies( true );
	    $editor->setAllowHtml( false );
	    
	    if( method_exists( $editor, 'setForceRte' ) )
	    {
	        $editor->setForceRte( true );
	    }
	    
	    if( method_exists( $editor, 'setRteEnabled' ) )
	    {
	        $editor->setRteEnabled( true );
	    }
	    
	    if( method_exists( $editor, 'setLegacyMode' ) )
	    {
	        $editor->setLegacyMode( false );
	    }
	
	    /* Set content in editor */
	    $edytor = $editor->show( 'Post', array(), $db[ 'content' ] );
		
		/* Koniec pobierania, konfiguracja końcowa. */
		$template = $this->registry->output->getTemplate('panel')->panel_admin_tickets($row, $posts, $edytor);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('Ticket #'.$row['t_uid'].'');
		$this->registry->output->addNavigation( 'Ticket #'.$row['t_uid'].'', 'app=panel&module=admin&section=tickets&uid='.$this->request['uid'].'' );
		$this->registry->getClass('output')->sendOutput();
	}	
}
?> 