<?php
class public_panel_admin_app extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 12 && $this->memberData['member_group_id'] != 6 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->silentRedirect('index.php');
		}
		
		$this->DB->query('SELECT * FROM `panel_applications` WHERE `status` = 1 ORDER by `a_uid`');	
		$row = $this->DB->fetch();

		if(!$row)
		{
			$this->registry->output->showError('Nie ma żadnych oczekujących aplikacji w systemie.',0);
		}
		
		if(isset($this->request['submit']))
		{
			if($this->request['choice'] == 2) 
			{
				$this->DB->query('UPDATE `fc_characters` SET `block` = 0 WHERE `player_uid` = '.$row['pid'].'');
			}
			$this->DB->query('UPDATE `panel_applications` SET `status` = '.$_POST['choice'].', `notes` = "'.$this->request['note'].'", `checkedby` = '.$this->memberData['member_id'].', `kolejka` = 0 WHERE `a_uid` = '.$this->request['uid'].'');
			
			//Kolejka
			$this->DB->query('UPDATE `panel_applications` SET `kolejka` = `kolejka` - 1 WHERE `kolejka` > 1');
			
			$this->DB->query('SELECT * FROM `fc_characters` WHERE `player_uid` = '.$row['pid'].'');	
			$char = $this->DB->fetch();
			
			//Powiadomienia
			//$this->DB->query('INSERT INTO `ipb_inline_notifications` (`notify_to_id`, `notify_sent`, `notify_read`, `notify_title`, `notify_text`, `notify_from_id`, `notify_type_key`, `notify_url`) VALUES ('.$row['owner_app'].', '.IPS_UNIX_TIME_NOW.', 0, "'.$this->memberData['members_display_name'].' odrzucił Twoją aplikację na postać: '.$char['name'].'.", "'.$this->memberData['members_display_name'].' zaakceptował Twoją aplikację na postać: '.$char['name'].'.", '.$this->memberData['member_id'].', "followed_topics", "http://sc-rp.pl/index.php?app=panel&module=chars")');
			
			//$this->DB->query('UPDATE `ipb_members` SET `notification_cnt` = `notification_cnt` + 1 WHERE `member_id` = '.$row['owner_app'].'');
			
			$this->registry->output->silentRedirect(
				$this->registry->output->buildUrl('/index.php?app=panel&module=admin&section=app','publicWithApp')
			);
		}
		
		$template = $this->registry->output->getTemplate('panel')->panel_admin_app($row);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('ACP');
		$this->registry->output->addNavigation( 'ACP', 'app=panel&module=admin&section=app' );
		$this->registry->getClass('output')->sendOutput();
	}
	
}
?>