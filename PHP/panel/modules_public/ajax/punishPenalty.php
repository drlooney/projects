<?php
class public_panel_ajax_punishPenalty extends ipsAjaxCommand 
{
	
	public function doExecute( ipsRegistry $registry ) 
	{			
		if($this->memberData['member_group_id'] != 4)
		{
			$this->registry->output->showError('Nie posiadasz uprawnień do zdjęcia kary.', 0);
		}
		
		if(!$this->request['penaltyID'])
		{
			$this->registry->output->showError('Dane dotyczące kary zostały źle pobrane, spróbuj ponownie.', 0);
		}
		
		$this->DB->query('SELECT * FROM `fc_penalty` WHERE `uid` = '.strval($this->request['penaltyID']).'');
		$row = $this->DB->fetch();
		
		if($row['type'] == 3) // Ban
		{
			$this->DB->query('UPDATE `fc_players` SET `block` = 0 WHERE `player_uid` = '.strval($row['player_uid']).'');
			$this->DB->query('DELETE FROM `fc_bans` WHERE `owner` = '.strval($row['global']).'');
		}
		else if($row['type'] == 3) // Admin Jail
		{
			$this->DB->query('UPDATE `fc_players` SET `aj` = 0 WHERE `player_uid` = '.strval($row['player_uid']).'');
		}
		else if($row['type'] == 3) // Blokada postaci
		{
			$this->DB->query('UPDATE `fc_players` SET `block` = `block` - 1 WHERE `player_uid` = '.strval($row['player_uid']).'');
		}
		else if($row['type'] == 3) // Blokada biegania
		{
			$this->DB->query('UPDATE `fc_players` SET `block` = `block` - 8 WHERE `player_uid` = '.strval($row['player_uid']).'');
		}
		else if($row['type'] == 3) // Blokada prowadzenia pojazdów
		{
			$this->DB->query('UPDATE `fc_players` SET `block` = `block` - 2 WHERE `player_uid` = '.strval($row['player_uid']).'');
		}
		else if($row['type'] == 3) // Blokada czatu OOC
		{
			$this->DB->query('UPDATE `fc_players` SET `block` = `block` - 4 WHERE `player_uid` = '.strval($row['player_uid']).'');
		}
		else if($row['type'] == 3) // Blokada broni
		{
			$this->DB->query('UPDATE `fc_players` SET `block` = `block` - 16 WHERE `player_uid` = '.strval($row['player_uid']).'');
		}
		
		$this->DB->query('UPDATE `fc_penalty` SET `expire` = 1 WHERE `uid` = '.$row['uid'].'');
		if($this->request['redirect'] == 1) $this->registry->output->silentRedirect('index.php?showuser='.$row['global'].'&tab=gameCard');
		else $this->registry->output->silentRedirect('index.php?app=panel&module=admin&section=penalty');
	}
}
?>