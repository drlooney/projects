<?php
class public_panel_ajax_blockchar extends ipsAjaxCommand
{

	public function doExecute( ipsRegistry $registry ) 
	{	
		if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->showError('Nie posiadasz uprawnień do zablokowania postaci lub nie wybrałeś żadnej z postaci.', 0);
		}
		
		$this->DB->query("SELECT * FROM `fc_players` WHERE `player_uid` = ".$this->request['char_id']."");
		$row = $this->DB->fetch();
		
				
		if(isset($this->request['block_char_submit']) && isset($this->request['block_status']))
		{
			$this->DB->query("UPDATE `fc_players` SET `block` = ".$this->request['block_status']." WHERE `player_uid` = ".$this->request['char_id']."");
			$this->registry->output->silentRedirect('index.php?app=panel&module=admin&section=char&char_id='.$this->request['char_id'].'');
		}
		
		if(isset($this->request['block_char_cancel']))
		{
			$this->registry->output->silentRedirect('index.php?app=panel&module=admin&section=char&char_id='.$this->request['char_id'].'');
		}	
		
		$this->returnHtml($this->registry->output->getTemplate('panel')->panel_ajax_blockchar($row));
	}
}
?>