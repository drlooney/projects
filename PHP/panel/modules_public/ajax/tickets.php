<?php
class public_panel_tickets_tickets extends ipsCommand
{
	public function doExecute( ipsRegistry $registry ) 
	{	
		/* Zabezpieczenia. */
		if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Nie jesteś zalogowany.',0);
		}
		if($this->request['type'] == 1 || $this->request['type'] == 2 || $this->request['type'] == 3)
		{
			if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 7)
			{
				$this->registry->output->silentRedirect('index.php?app=panel&module=game&section=tickets');
			}
		}
		/* Paginacja */
		if(!$this->request['type'])
		{
			$count = $this->DB->query('SELECT COUNT(*) as max FROM `tickets` WHERE `t_starter_id` = '.$this->memberData['member_id'].'');
		}
		else if($this->request['type'] == 1)
		{
			$count = $this->DB->query('SELECT COUNT(*) as max FROM `tickets` WHERE `admin` = '.$this->memberData['member_id'].' AND status != 5');
		}
		else if($this->request['type'] == 2)
		{
			$count = $this->DB->query('SELECT COUNT(*) as max FROM `tickets` WHERE `admin` = 0 AND status != 5');
		}
		else if($this->request['type'] == 3)
		{
			$count = $this->DB->query('SELECT COUNT(*) as max FROM `tickets`');
		}
		$count = $this->DB->fetch($count);
		$pagination = $this->registry->getClass('output')->generatePagination( array( 
															'totalItems'		=> $count['max'],
															'itemsPerPage'		=> 10,
															'baseUrl'			=> "app=panel&modules=tickets&section=tickets&type=".$this->request['type']."",
															)
														);
		/* Pobieranie danych dot. podglądanego ticketa. */
		if(!$this->request['type'])
		{
			$page = 0;
			$this->DB->query(sprintf('SELECT * FROM `tickets` WHERE `t_starter_id` = '.$this->memberData['member_id'].'  ORDER by `t_uid` DESC LIMIT %d,10',$this->request['st']));
		}
		else if($this->request['type'] == 1)
		{
			$page = 1;
			$this->DB->query(sprintf('SELECT * FROM `tickets` WHERE `admin` = '.$this->memberData['member_id'].' AND status != 5 ORDER by `t_uid` DESC LIMIT %d,10',$this->request['st']));
		}
		else if($this->request['type'] == 2)
		{
			$page = 2;
			$this->DB->query(sprintf('SELECT * FROM `tickets` WHERE `admin` = 0 AND status != 5 ORDER by `t_uid` DESC LIMIT %d,10',$this->request['st']));
		}
		else if($this->request['type'] == 3)
		{
			$page = 3;			
			$this->DB->query(sprintf('SELECT * FROM `tickets` ORDER by `t_uid` DESC LIMIT %d,10',$this->request['st']));	
		}
		$this->DB->execute();	
		while($row = $this->DB->fetch())
		{	
			switch($row['t_type'])
			{
				case 0: $row['t_type'] = 'Niesprecyzowany'; break;
				case 1: $row['t_type'] = 'Dotyczy pojazdu'; break;
				case 2: $row['t_type'] = 'Dotyczy drzwi'; break;
				case 3: $row['t_type'] = 'Dotyczy grupy'; break;
				case 4: $row['t_type'] = 'Dotyczy pod-grupy'; break;
				case 7: $row['t_type'] = 'Dotyczy moderatora'; break;
				case 8: $row['t_type'] = 'Dotyczy administratora'; break;
				case 9: $row['t_type'] = 'Dotyczy działu / tematu'; break;
				case 10: $row['t_type'] = 'Dotyczy paneli gracza'; break;
				case 11: $row['t_type'] = 'Dotyczy błędu na forum'; break;
				case 12: $row['t_type'] = 'Dotyczy błędu w grze'; break;
			}
			switch($row['status'])
			{
				case 0: $row['status'] = 'Nowy'; break;
				case 1: $row['status'] = 'Przyjęty'; break;
				case 2: $row['status'] = 'Konwersacja'; break;
				case 3: $row['status'] = 'Realizacja'; break;
				case 4: $row['status'] = 'Rozwiązany'; break;
				case 5: $row['status'] = 'Zakończony'; break;
			}
			$rows[] = $row;
		}
		
		/* Koniec pobierania, konfiguracja końcowa. */
		$template = $this->registry->output->getTemplate('panel')->panel_tickets($rows, $posts, $page, $pagination);
		$this->registry->getClass('output')->addContent($template);
		$this->registry->output->setTitle('Zgłoszenia');
		$this->registry->output->addNavigation( 'Zgłoszenia', 'index.php?app=panel&modules=tickets&section=tickets' );
		/* Zgłoszenia */
		if($page == 0)
		$this->registry->output->addNavigation( 'Twoje zgłoszenia', 'index.php?app=panel&modules=tickets&section=tickets' );
		else if($page == 1)
		$this->registry->output->addNavigation( 'Zgłoszenia, którymi się zajmujesz', 'index.php?app=panel&modules=tickets&section=tickets&type=1' );
		else if($page == 2)
		$this->registry->output->addNavigation( 'Nowe', 'index.php?app=panel&modules=tickets&section=tickets&type=2' );
		else if($page == 3)
		$this->registry->output->addNavigation( 'Wszystkie', 'index.php?app=panel&modules=tickets&section=tickets&type=3' );
		/* koniec */
		$this->registry->getClass('output')->sendOutput();
	}	
}
?> 