<?php
class public_panel_ajax_ticketadd extends ipsAjaxCommand
{

	public function doExecute( ipsRegistry $registry ) 
	{	
		if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Nie jesteś zalogowany.',0);
		}
		/* Zapis odpowiedzi */
		$_POST[ 'Post' ]    = IPSText::stripslashes( $_POST[ 'Post' ] );
 
	    $classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/editor/composite.php', 'classes_editor_composite' );
	    $editor = new $classToLoad();
	
	    $editor->setAllowBbcode( true );
	    $editor->setAllowSmilies( true );
	    $editor->setAllowHtml( false );
	    
	    if( method_exists( $editor, 'setForceRte' ) )
	    {
	        $editor->setForceRte( true );
	    }
	     
	    if( method_exists( $editor, 'setRteEnabled' ) )
	    {
	        $editor->setRteEnabled( true );
	    }
	     
	    if( method_exists( $editor, 'setLegacyMode' ) )
	    {
	        $editor->setLegacyMode( false );
	    }
 
		$content = $editor->process( $_POST[ 'Post' ] );
	
	    IPSText::getTextClass('bbcode')->parse_html			= 0;
	    IPSText::getTextClass('bbcode')->parse_nl2br		= 0;
	    IPSText::getTextClass('bbcode')->parse_smilies		= 1;
	    IPSText::getTextClass('bbcode')->parse_bbcode		= 1;
	    IPSText::getTextClass('bbcode')->parsing_section	= 'content';
	
	    $content = IPSText::getTextClass('bbcode')->preDbParse( $content );
		
		if(isset($this->request['submit']))
		{
			$this->DB->insert( 'tickets', array( 't_start_message' => $content, 't_date' => time(), 't_starter_id' => $this->memberData['member_id'], 't_topic' => $this->request['title'], 't_type' => $this->request['type'] ) ); 
		}	
		$this->registry->output->silentRedirect('index.php?app=panel&module=tickets&section=tickets');
	}
}
?>