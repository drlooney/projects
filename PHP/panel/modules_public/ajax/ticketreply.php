<?php
class public_panel_ajax_ticketreply extends ipsAjaxCommand
{

	public function doExecute( ipsRegistry $registry ) 
	{	
		if(!$this->memberData['member_id'])
		{
			$this->registry->output->showError('Nie jesteś zalogowany.',0);
		}
		/* pobieranie ticketa */
		$this->DB->query("SELECT * FROM `ipb_tickets` WHERE `t_uid` = ".$this->request['ticket_id']." LIMIT 1");	
		$row = $this->DB->fetch();
		/* Zapis odpowiedzi */
		$_POST[ 'Post' ]    = IPSText::stripslashes( $_POST[ 'Post' ] );
 
	    $classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/editor/composite.php', 'classes_editor_composite' );
	    $editor = new $classToLoad();
	
	    $editor->setAllowBbcode( true );
	    $editor->setAllowSmilies( true );
	    $editor->setAllowHtml( false );
	    
	    if( method_exists( $editor, 'setForceRte' ) )
	    {
	        $editor->setForceRte( true );
	    }
	     
	    if( method_exists( $editor, 'setRteEnabled' ) )
	    {
	        $editor->setRteEnabled( true );
	    }
	     
	    if( method_exists( $editor, 'setLegacyMode' ) )
	    {
	        $editor->setLegacyMode( false );
	    }
 
		$content = $editor->process( $_POST[ 'Post' ] );
	
	    IPSText::getTextClass('bbcode')->parse_html			= 0;
	    IPSText::getTextClass('bbcode')->parse_nl2br		= 0;
	    IPSText::getTextClass('bbcode')->parse_smilies		= 1;
	    IPSText::getTextClass('bbcode')->parse_bbcode		= 1;
	    IPSText::getTextClass('bbcode')->parsing_section	= 'content';
	
	    $content = IPSText::getTextClass('bbcode')->preDbParse( $content );
		
		if(isset($this->request['ticket_reply']))
		{
			$this->DB->insert( 'tickets_post', array( 'ticket_uid' => $this->request['ticket_id'], 'posts' => $content, 'date' => time(), 't_reply_id' => $this->memberData['member_id'] ) ); 
			/* Dodaj powiadomienie */
			if($row['admin'] == $this->memberData['member_id'])
			{
				$url = 'index.php?app=panel&module=admin&section=tickets&uid='.$row['t_uid'].'&owner='.$row['t_starter_id'].'';
				$notify = 'Ticket #'.$row['t_uid'].', nowa odpowiedź. <a href="index.php?app=panel&module=admin&section=tickets&uid='.$row['t_uid'].'&owner='.$row['t_starter_id'].'">Zobacz</a>';
				$this->DB->insert( 'inline_notifications', array( 'notify_from_id' => $this->memberData['member_id'], 'notify_title' => $notify, 'notify_sent' => time(), 'notify_text' => $notify, 'notify_type_key' => 'profile_comment', 'notify_to_id' => $row['t_starter_id'], 'notify_read' => 0, 'notify_url' => $url) ); 
			}
			else
			{
				$url = 'index.php?app=panel&module=admin&section=tickets&uid='.$row['t_uid'].'';
				$notify = 'Ticket #'.$row['t_uid'].', nowa odpowiedź. <a href="index.php?app=panel&module=admin&section=tickets&uid='.$row['t_uid'].'">Zobacz</a>';
				$this->DB->insert( 'inline_notifications', array( 'notify_from_id' => $this->memberData['member_id'], 'notify_title' => $notify, 'notify_sent' => time(), 'notify_text' => $notify, 'notify_type_key' => 'profile_comment', 'notify_to_id' => $row['admin'], 'notify_read' => 0, 'notify_url' => $url) ); 			
			}
		}	
		$this->registry->output->silentRedirect('index.php?app=panel&module=admin&section=tickets&uid='.$this->request['ticket_id'].'&owner='.$row['t_starter_id'].'');
	}
}
?>