<?php

class public_panel_ajax_info extends ipsAjaxCommand 
{
	public function doExecute( ipsRegistry $registry ) 
	{
	if($this->memberData['member_group_id'] != 4 && $this->memberData['member_group_id'] != 7)
		{
			$this->registry->output->silentRedirect('index.php');
		}
		
		$count = $this->DB->query('SELECT COUNT(*) as max FROM `panel_applications`');
		$count = $this->DB->fetch($count);
		
		/* Parsowanie paginacji */
		$pagination = $this->registry->getClass('output')->generatePagination( array( 
																		'totalItems'		=> $count['max'],
																		'itemsPerPage'		=> 10,
																		'baseUrl'			=> "app=panel&module=admin&section=applogs",
																		)
																);
		
		$this->DB->query(sprintf('SELECT * FROM `panel_applications` ORDER by `a_uid` ASC LIMIT %d,10',$this->request['st']));	
		$this->DB->execute();	

		while($row = $this->DB->fetch())
		{     
            switch($row['status'])
			{
                case 0: $row['status'] = 'Brak'; break;
    			case 1: $row['status'] = 'Oczekuje'; break;
				case 2: $row['status'] = 'Zaakceptowana'; break;
				case 3: $row['status'] = 'Odrzucona'; break;
				case 4: $row['status'] = 'Zablokowana'; break;
				default: $row['status'] = 'Błąd systemowy'; break;
			}
            		
			$applogs[] = $row;
	
		}
	
	
	
	
	
	
		$this->returnHtml($this->registry->output->getTemplate('panel')->ajax_show($this->request['string'], $row, $pagination, $applogs));
	}
}
?>