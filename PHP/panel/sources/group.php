<?php

class group
{
	protected $registry;
	protected $DB;
	protected $settings;
	protected $request;
	protected $memberData;

	public function __construct( ipsRegistry $registry )
	{
		$this->registry		=  $registry;
		$this->DB			=  $this->registry->DB();
		$this->settings		=& $this->registry->fetchSettings();
		$this->request		=& $this->registry->fetchRequest();
		$this->memberData	=& $this->registry->member()->fetchMemberData();
	}
	
	public function fetchGroupInformation( $group )
	{
		$this->DB->query('SELECT * FROM fc_groups WHERE uid = '. $group .'');
		$row = $this->DB->fetch();
		
		$kind = $this->getGroupType();
		
		$row['kind_name'] = $kind[$row['kind']];
		
		return $row;
	}
	
	public function checkMemberPermissions( $group, $character )
	{
		$this->DB->query("SELECT g.leader, m.group_id, IFNULL( r.name,  'Brak' ) AS name, IFNULL( r.permissions, 0 ) AS permissions, IFNULL( r.skin, 0 ) AS skin, IFNULL( r.payout, 0 ) AS payout, m.subgroup, m.time FROM fc_member_groups m LEFT JOIN fc_groups g ON ( g.uid = m.group_id )  LEFT JOIN fc_ranks r ON ( r.uid = m.rank_uid)  WHERE m.player_uid = " . $character . " AND g.uid = " . $group . "");
		$row = $this->DB->fetch();

		if( $row )
		{
			if( $row['leader'] == $character || $row['permissions'] & 1 )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{	
			return false;
		}
	}
	
	public function checkOwnerCharacter( $character, $member )
	{
		$this->DB->query('SELECT player_uid FROM fc_characters WHERE player_uid = '. $character .' AND global_uid = '. $member .'');
		
		if( $this->DB->getTotalRows() )
		{
			return true;
		}
		else
		{
			return false;	
		}
	}
	
	public function checkCharacterAndGroup( $group, $character, $member )
	{
		if( $this->memberData['member_group_id'] != 4 )
		{
			if( ! $this->checkMemberPermissions( $group, $character ) )
			{
				$this->registry->output->showError( 'Brak uprawnień do przeglądania panelu tej grupy.' );
			}
			
			if( ! $this->checkOwnerCharacter( $character, $member ) )
			{
				$this->registry->output->showError( 'Ta postać nie należy do Ciebie.' );
			}
		}

		return true;
	}

	public function getGroupRanks( $group )
	{
		$this->DB->query('SELECT * FROM `fc_ranks` WHERE `group_uid` = ' . $group . '');

		return $this->DB->getTotalRows();
	}
	
	public function getGroupType()
	{
		return array( '1'	=>	'Porządkowa',
					  '2'	=>	'Federalna',
					  '3'	=>	'Ochrona rządu',
					  '4'	=>	'Urzędnicza',
					  '5'	=>	'Medyczna',
					  '6'	=>	'Straż pożarna',
					  '7'	=>	'Radio',
					  '8'	=>	'Mafia',
					  '9'	=>	'Gang motocyklowy',
					  '10'	=>	'Ściganci',
					  '11'	=>	'Syndykat',
					  '12'	=>	'Sklep 24/7' );
	}

	public function GetVehicleName( $modelid )
	{
		$vehicle = array(
							579 => 'Huntley',
							400 => 'Landstalker',
							404 => 'Perrenial',
							489 => 'Rancher',
							505 => 'Rancher',
							479 => 'Regina',
							442 => 'Romero',
							458 => 'Solair',
							602 => 'Alpha',
							496 => 'Blista',
							401 => 'Bravura',
							518 => 'Buccaneer',
							527 => 'Cadrona',
							589 => 'Club',
							419 => 'Esperanto',
							533 => 'Feltzer',
							526 => 'Fortune',
							474 => 'Hermes',
							545 => 'Hustler',
							517 => 'Majestic',
							410 => 'Manana',
							600 => 'Picador',
							436 => 'Previon',
							580 => 'Stafford',
							439 => 'Stallion',
							549 => 'Tampa',
							491 => 'Virgo',
							445 => 'Admiral',
							507 => 'Elegant',
							585 => 'Emperor',
							587 => 'Euros',
							466 => 'Glendale',
							492 => 'Greenwood',
							546 => 'Intruder',
							551 => 'Merit',
							516 => 'Nebula',
							467 => 'Oceanic',
							426 => 'Premier',
							547 => 'Primo',
							405 => 'Sentinel',
							409 => 'Stretch',
							550 => 'Sunrise',
							566 => 'Tahoma',
							540 => 'Vincent',
							421 => 'Washington',
							529 => 'Willard',
							402 => 'Buffalo',
							542 => 'Clover',
							603 => 'Phoenix',
							475 => 'Sabre',
							562 => 'Elegy',
							565 => 'Flash',
							559 => 'Jester',
							561 => 'Stratum',
							560 => 'Sultan',
							558 => 'Uranus',
							429 => 'Banshee',
							541 => 'Bullet',
							415 => 'Cheetah',
							480 => 'Comet',
							434 => 'Hotknife',
							494 => 'Hotring',
							502 => 'Hotring A',
							503 => 'Hotring B',
							411 => 'Infernus',
							506 => 'Super GT',
							451 => 'Turismo',
							555 => 'Windsor',
							477 => 'ZR-350',
							435 => 'Artict1 (Semi Trailer)',
							450 => 'Artict2 (Semi Trailer)',
							591 => 'Artict3 (Semi Trailer)',
							584 => 'Xoomer (Gas Tanker Trailer)',
							499 => 'Benson',
							498 => 'Boxville',
							609 => 'Boxville (Black for Robbery)',
							524 => 'Cement Truck',
							532 => 'Combine Harvestor',
							578 => 'DFT-30',
							486 => 'Dozer',
							406 => 'Dumper',
							573 => 'Dune',
							455 => 'Flatbed',
							588 => 'Hotdog',
							403 => 'Linerunner',
							423 => 'Mr Woopee',
							414 => 'Mule',
							443 => 'Packer',
							515 => 'Roadtrain',
							514 => 'Tanker',
							531 => 'Tractor',
							610 => 'Farm Trailer',
							456 => 'Yankee',
							459 => 'Topfun (RC Van)',
							422 => 'Bobcat',
							482 => 'Burrito',
							530 => 'Forklift',
							418 => 'Moonbeam',
							572 => 'Mower',
							582 => 'Newsvan',
							413 => 'Pony',
							440 => 'Rumpo',
							543 => 'Sadler',
							583 => 'Tug',
							478 => 'Walton',
							554 => 'Yosemite',
							536 => 'Blade',
							575 => 'Broadway',
							534 => 'Remington',
							567 => 'Savanna',
							535 => 'Slamvan',
							576 => 'Tornado',
							412 => 'Voodoo',
							568 => 'Bandito',
							424 => 'BF Injection',
							504 => 'Bloodring Banger',
							457 => 'Caddy',
							483 => 'Camper',
							508 => 'Journey',
							571 => 'Kart',
							500 => 'Mesa',
							444 => 'Monster',
							556 => 'Monstera',
							557 => 'Monsterb',
							471 => 'Quad',
							495 => 'Sandking',
							539 => 'Vortex',
							481 => 'Bmx',
							509 => 'Bike',
							510 => 'Mountain Bike',
							581 => 'BF-400',
							462 => 'Faggio',
							521 => 'FCR-900',
							463 => 'Freeway',
							522 => 'NRG-500',
							461 => 'PCJ-600',
							448 => 'Pizzaboy',
							468 => 'Sanchez',
							586 => 'Wayfarer',
							606 => 'Bagboxa (Baggage Trailer)',
							607 => 'Bagboxb (Baggage Trailer)',
							485 => 'Baggage',
							431 => 'Bus',
							438 => 'Cabbie',
							437 => 'Coach',
							574 => 'Sweeper',
							611 => 'Sweeper Trailer',
							420 => 'Taxi',
							525 => 'Towtruck',
							408 => 'Trashmaster',
							608 => 'Tug Stairs (Stairs Trailer)',
							552 => 'Utility Van',
							416 => 'Ambulance',
							433 => 'Barracks',
							427 => 'Enforcer',
							490 => 'FBI Rancher',
							528 => 'FBI Truck',
							407 => 'Fire Truck',
							544 => 'Fire Truck A',
							523 => 'HPV-1000 (Police Bike)',
							470 => 'Patriot',
							596 => 'Police Los Santos',
							597 => 'Police San Fierro',
							598 => 'Police Las Venturas',
							599 => 'Police Ranger',
							432 => 'Rhino',
							428 => 'Securicar',
							601 => 'Swat Tank',
							592 => 'Andromada',
							577 => 'AT-400',
							511 => 'Beagle',
							548 => 'Cargobob',
							512 => 'Cropduster',
							593 => 'Dodo',
							425 => 'Hunter',
							417 => 'Leviathon',
							487 => 'Maverick',
							553 => 'Nevada',
							488 => 'News Maverick',
							497 => 'Police Maverick',
							563 => 'Raindance',
							476 => 'Rustler',
							447 => 'Seasparrow',
							519 => 'Shamal',
							460 => 'Skimmer',
							469 => 'Sparrow',
							513 => 'Stunt Plane',
							520 => 'Hydra',
							472 => 'Coastguard',
							473 => 'Dingy',
							493 => 'Jetmax',
							595 => 'Launch',
							484 => 'Marquis',
							430 => 'Predator',
							453 => 'Reefer',
							452 => 'Speeder',
							446 => 'Squallo',
							454 => 'Tropic',
							441 => 'RC Bandit',
							464 => 'RC Baron',
							594 => 'RC Cam',
							465 => 'RC Goblin',
							501 => 'RC Goblin',
							564 => 'RC Tigerv',
							604 => 'Glendale',
							605 => 'Sadler'
			);
			
			$model = $vehicle[ $modelid ];

			return $model;
	}
}