<?php

class profile_player extends profile_plugin_parent
{
	/**
	 * Return HTML block
	 *
	 * @access	public
	 * @param	array		Member information
	 * @return	string		HTML block
	 */
	public function return_html_block( $member = array() )
	{
		$characters = $this->fetchCharacters( $member['member_id'] );
		$logs = $this->fetchLogs( $member['member_id'] );		
		
		return $this->registry->getClass( 'output' )->getTemplate( 'panel' )->profileCard( $characters, $logs, $member );
	}
	
	protected function fetchCharacters( $member )
	{
		$this->DB->query( 'SELECT * FROM `fc_characters` WHERE `global_uid` = '. $member );
		
		while( $row = $this->DB->fetch() )
		{
			$row['name'] = str_replace( '_', ' ', $row['name'] );
			$characters[] = $row;
		}
		
		return $characters;
	}
	
	protected function fetchLogs( $member )
	{
		$this->DB->query( 'SELECT p.*, m.members_display_name FROM fc_penalty p, ipb_members m 
							WHERE p.penalty_admin_global_id = m.member_id AND p.penalty_user_global_id = '. $member );
		
		while( $row = $this->DB->fetch() )
		{
			$row['penalty_type'] = $this->getPenaltyName( $row['penalty_type'] );
			$logs[] = $row;
		}
		
		return $logs;
	}
	
	protected function getPenaltyName( $type )
	{
		switch( $type )
		{
			case 1:
			 $name = 'Warn';
			 break;
			case 2:
			 $name = 'Kick';
             break;
			case 3:
     		 $name = 'Banicja';
			 break;
			case 4:
             $name = 'Blokada postaci';
			 break;
			case 5:
             $name = 'Punkty';
			 break;
			case 6:
             $name = 'Admin Jail'; 
			 break;
		}
		
		return $name;
	}
}